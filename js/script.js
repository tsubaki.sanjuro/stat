$(document).ready(function () {
	$(".statistics-btn").click(function () {
		const tabindex = $(".statistics-btn").index(this);
		switchTabs(this, "btn-act", "frame-content", tabindex);

	});
	$(".statistics-btn").hover(
		function () {
			HvrBtn(this, 'shadow')
		},
		function () {
			const tabindex = $(".statistics-btn").index(this);
			RemShad(this, 'shadow', tabindex)
		}
	)


	function switchTabs(elem, switchClass, contentClass, index) {
		$(elem).addClass(switchClass).siblings().removeClass(switchClass);
		$(".frame-content").removeClass("active").eq(index).addClass("active");
	}

	function HvrBtn(elem, addclass = "shadow") {
		console.log(elem, addclass)
		$(elem).addClass(addclass).siblings().removeClass(addclass)
	}

	function RemShad(elem, remclass = "shadow", index) {
		console.log(elem, remclass)
		$(elem).removeClass(remclass).eq(index).addClass(remclass)
	}
})